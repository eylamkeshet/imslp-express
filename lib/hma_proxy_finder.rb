module HMAProxyFinder
  require 'nokogiri'
  require 'mechanize'

  def find_proxies
    agent = Mechanize.new
    page = agent.get('http://proxylist.hidemyass.com/')
    rows = page.css('.altshade')
    ip_array = []
    rows.each do |row|
      ip_cell = row.css('td')[1]
      hidden_classes = (ip_cell.text.strip).scan(/\.-?[_a-zA-Z]+[_a-zA-Z0-9-]*\s*{display:inline}/)
      ip_cell.css('span').each_with_index do |span,index|
        if(index!=0)
          span_style = ""
          span_class = ""
          fake_span = true
          if(span.attribute('style'))
            span_style = span.attribute('style').text
          end
          if(span.attribute('class'))
            span_class = span.attribute('class').text
          end

          if(span_style == 'display: inline' || /\A[-+]?\d+\z/ === span_class)
            ip_array << span.text.strip
          else
            hidden_classes.each do |cl|
              if (!span_class.empty? && (cl.include?(span_class)))
                fake_span = false
              end
            end
            if(!fake_span)
              ip_array << span.text.strip
            end
          end
        end
       end

      port_cell = row.css('td')[2]
    end

    def is_integer? string
      true if Integer(string) rescue false
    end

  end
end