class ScoreController < ApplicationController
  #encoding: UTF-8
  require 'open-uri'
  require 'nokogiri'
  require 'mechanize'
  require 'json'

  def index
    # Array with IMSLP results and subresults* (*Every link inside the result)
    @imslp_results = []
    # Agent for opening web pages
    agent = Mechanize.new

    # Randomizing the user agent
    user_agents = {  'Linux Firefox' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:43.0) Gecko/20100101 Firefox/43.0',
                     'Linux Konqueror' => 'Mozilla/5.0 (compatible; Konqueror/3; Linux)',
                     'Linux Mozilla' => 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.4) Gecko/20030624',
                     'Mac Firefox' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0',
                     'Mac Mozilla' => 'Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.4a) Gecko/20030401',
                     'Mac Safari 4' => 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; de-at) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10',
                     'Mac Safari' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
                     'Windows Chrome' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.125 Safari/537.36',
                     'Windows IE 6' => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)',
                     'Windows IE 7' => 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
                     'Windows IE 8' => 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
                     'Windows IE 9' => 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)',
                     'Windows IE 10' => 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)',
                     'Windows IE 11' => 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
                     'Windows Edge' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586',
                     'Windows Mozilla' => 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4b) Gecko/20030516 Mozilla Firebird/0.6',
                     'Windows Firefox' => 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0' }
    random_agent = user_agents[rand(user_agents.size)]
    agent.user_agent = random_agent
    # Search param
    params.has_key?(:search)? search_parameter = params[:search] : return
    # Sleep a random time of seconds to appear as human
    sleep(rand(5..10))
    # Open Google query with the searched param, by submit it and behaving like human
    google_results = agent.get('http://www.google.com/')
    google_form = google_results.form('f')
    google_form.q = "site:imslp.org " + search_parameter
    google_results = agent.submit(google_form, google_form.buttons.first)

    # Regex to get IMSLP links from google
    google_imslp_re = /\?q=http:\/\/imslp.org\/wiki/

    # Filtering the results (without the #sub-categories links bug)
    google_results = google_results.css("h3.r a").select{ |n| n['href'] =~ google_imslp_re }

    google_results.each do |link|
     # Format URL from Google results
     formatted_link = /(?<=url\?q=)(.*)(?=&sa)/.match(URI.decode(link['href']))
     # Hash of IMSLP subresults
     imslp_subresults = []
     # Open the IMSLP page from the google results
     imslp_page = agent.get(formatted_link)
     # Get the div containing the scores
     imslp_page.search('//div[starts-with(@id, "tab")]').each do |e|
       if(!e.text.strip.include?("Recordings") && !e.text.strip.include?("MIDI"))

         tab_links = []
         tab_title = ""

         # Fetch every "we" class from tabs - get the edition info and organize the uls
         e.css(".we").each do |elem|

           edition_links = []
           edition_info = []
           # Add to edition info array the category and the info
           elem.css(".we_edition_entry").map do |i|

             # Edition info category
             info_category = i.previous_element.text.gsub(":","")
             if(info_category.downcase != "purchase" && info_category.downcase != "copyright")
               # Inserting edition info in an array which includes category and info
               edition_info << [info_category, i.text]
             end
           end

           if(elem.previous_element.node_name == "h5")
             edition_info.unshift(["Arranged for", elem.previous_element.text.strip.gsub("For ","")])
           end

           # Regex that matches the download links
           imslp_download_re = /Special:ImagefromIndex/
           # Insert the links+their size in MB and edition info
           links_array = elem.css('a').select{ |n| n['href'] =~ imslp_download_re }
           links_and_size_array = []
           links_array.each do |link|
             size = /\d*\.?\d*MB/.match(link.parent.parent.parent.parent.text.strip)
             links_and_size_array <<  [link,size]
           end

           edition_links << [edition_info, links_and_size_array]
           # Tab title
           tab_title = e.child.text.strip
           # Tab links array contain - Every edition links and info and all of the available headers
           tab_links << edition_links
         end
         # IMSLP Subresults array include - The tab title and the tab links
         imslp_subresults << [tab_title, tab_links]
       end
     end

     # Add to results only if scores are contained
     if(imslp_subresults.any?)
       # The return array contains the a substring of the title and an array of links
       @imslp_results << [imslp_page.title.gsub(" - IMSLP/Petrucci Music Library: Free Public Domain Sheet Music",""),imslp_subresults]
     end
    end
  end

  def get_pdf
    agent = Mechanize.new
    url = params[:pdf_url]
    # Getting the page while sending a cookie that accept the terms
    url.sub! 'ImagefromIndex', 'IMSLPDisclaimerAccept'
    page = agent.post(url, {'imslpdisclaimeraccepted' => 'yes'})
    if(!!(page.uri.to_s =~ /\/linkhandler\.php\?path=/))
      #EU or CA links handling
      eu_url = (page.uri.to_s).gsub("linkhandler.php?path=","files")
      redirect_to eu_url
    else
      #Check if the link is already PDF or needs to go one level deeper
      parsed_json_headers = JSON.parse(page.response.to_json)
      if (parsed_json_headers['content-type'] != "text/html; charset=UTF-8")
        redirect_pdf = page.uri.to_s
      else
        redirect_pdf = page.at("#sm_dl_wait").attributes["data-id"].to_s
      end
      redirect_to redirect_pdf
    end
  end


end
